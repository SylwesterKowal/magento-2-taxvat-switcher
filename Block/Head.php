<?php


namespace Kowal\SwitchPriceNettoBrutto\Block;

/**
 * Class Head
 *
 * @package Kowal\SwitchPriceNettoBrutto\Block
 */
class Head extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Customer\Model\SessionFactory
     */
    protected $_customerSession;
    protected $_scopeConfig;

    /**
     * Head constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\SessionFactory $customerSession
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\SessionFactory $customerSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    )
    {
        $this->_customerSession = $customerSession;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getCustomerGroupId()
    {
        $customer = $this->_customerSession->create();
        return $customer->getCustomer()->getGroupId();
        //return __('Hello Developer! This how to get the storename: %1 and this is the way to build a url: %2', $this->_storeManager->getStore()->getName(), $this->getUrl('contacts'));
    }

    /**
     * @return mixed
     */
    public function getTaxDisplayType()
    {
        return $this->_scopeConfig->getValue('tax/display/type', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}

